package com.nalinthip.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank nalinthip = new BookBank("Nalinthip", 100.0); // Default Constructor
        nalinthip.print();
        nalinthip.deposit(50);
        nalinthip.print();
        nalinthip.withdraw(50);
        nalinthip.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
